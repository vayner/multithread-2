cmake_minimum_required(VERSION 2.8)

# --------------- #
# CMake Functions #
# --------------- #

# * GetLibraryRootDir
# * @param result 		The variable in which the result will be stored
# * @param libdir 		The library directory (for example "SDL2")
# * 
# * Assigns the root libray directory to $result. 
function(GetLibraryRootDir result libdir)
	set(${result} ${CMAKE_CURRENT_SOURCE_DIR}/lib/${libdir} PARENT_SCOPE)
endfunction(GetLibraryRootDir)

# * AddIncludeDir 
# * @param target 		The target to include the path
# * @param dir 			The directory to be included
# * 
# * Adds a directory for inclusion in the header search path.
function(AddIncludeDir target dir) 
    get_property(curdirs TARGET ${target} PROPERTY INCLUDE_DIRECTORIES)
    set_property(TARGET ${target} PROPERTY INCLUDE_DIRECTORIES 
                ${curdirs} ${dir})
endfunction(AddIncludeDir)

# * AddLibrary
# * @param target 		The target to link the library to
# * @param directory 	The top level directory name (the one in ./lib/)
# * @param library 		The name of the library (no extensions)
# * @param confSpec		Is the library configuration specific, i.e. "debug" 
# * 					and release? Only applies to Windows.
# *
# * Adds a library with name "library" to the target. Under WIN32, the full 
# * path to the library is used. The library is also linked to the target 
# * "unittests", as it requires all libraries included in the entire project.
# * The library binary MUST exist at the path:
# *     ./lib/${directory}/lib/${library}.lib
# *
# * Or, if "confSpec" is true, the two different versions of the library must
# * exist at the locations:
# * 	./lib/${directory}/lib/Debug/${library}.lib
# * 	./lib/${directory}/lib/Release/${library}.lib
# * 
# * Under Windows, the include path is added as well. It must exist
# * at the path:
# *     ./lib/${directory}/include
function(AddLibrary target directory library confSpec)
	if (WIN32)
		GetLibraryRootDir(librootdir ${directory})

		if (NOT confSpec)
			set(library "${librootdir}/lib/${library}.lib")
			if (NOT EXISTS ${library})
				message(FATAL_ERROR "Library not found: ${library}")
			endif()
			target_link_libraries(${target} ${library})
		else()
			# Debug
			set(debugLib "${librootdir}/lib/Debug/${library}.lib")
			if (NOT EXISTS ${debugLib})
				message(FATAL_ERROR "Library not found: ${debugLib}")
			endif()

			# Release
			set(releaseLib "${librootdir}/lib/Release/${library}.lib")
			if (NOT EXISTS ${releaseLib})
				message(FATAL_ERROR "Library not found: ${releaseLib}")
			endif()
			
			set(link optimized ${releaseLib} debug ${debugLib})
			target_link_libraries(${target} ${link})
		endif()

		AddIncludeDir(${target} "${librootdir}/include")
	else()
		target_link_libraries(${target} ${library})
	endif()
endfunction(AddLibrary)

# * CopyDll
# * @param directory	The top level directory name  
# * @param library 		The library name (for example SDL2_image)
# *
# * Copies a DLL from the library directory to the executable directory.
function(CopyDll directory library)
	if (WIN32)
		GetLibraryRootDir(libroot ${directory})
		set(libfile ${library}.dll)
		set(libpath ${libroot}/lib/${libfile})
		
		if (EXISTS ${libpath})
			# Copy the DLL to Debug and Release directories
			execute_process(COMMAND ${CMAKE_COMMAND} -E 
							copy ${libpath} ${EXECUTABLE_OUTPUT_PATH}/Debug/${libfile}
							RESULT_VARIABLE out1
							ERROR_VARIABLE err1)
			execute_process(COMMAND ${CMAKE_COMMAND} -E 
							copy ${libpath} ${EXECUTABLE_OUTPUT_PATH}/Release/${libfile}
							RESULT_VARIABLE out2
							ERROR_VARIABLE err2)
		else()
			message(FATAL_ERROR "File does not exist: ${libfile}")
		endif()
	endif(WIN32)
endfunction(CopyDll)


# ----------------- #
# Main CMake Script #
# ----------------- #

project(multithread-2)

set(EXECUTABLE_OUTPUT_PATH ${CMAKE_CURRENT_LIST_DIR}/build)

file(GLOB common_src "source/common/*.h" "source/common/*.cpp")
file(GLOB part1_src "source/part1/*.h" "source/part1/*.cpp")
file(GLOB part2v1_src "source/part2v1/*.h" "source/part2v1/*.cpp")
file(GLOB part2v2_src "source/part2v2/*.h" "source/part2v2/*.cpp")

add_executable(part1 ${part1_src} ${common_src})
add_executable(part2v1 ${part2v1_src} ${common_src})
add_executable(part2v2 ${part2v2_src} ${common_src})


# ------------------------------ #
#       Library Management       #
# ------------------------------ #

if (UNIX)
    AddLibrary(part1 pthread pthread false)
    AddLibrary(part2v1 pthread pthread false)
    AddLibrary(part2v2 pthread pthread false)
endif()

# ------------------------- #
#    Header search paths    #
# ------------------------- #

AddIncludeDir(part1 "${CMAKE_CURRENT_SOURCE_DIR}/source/common")
AddIncludeDir(part1 "${CMAKE_CURRENT_SOURCE_DIR}/source/part1")

AddIncludeDir(part2v1 "${CMAKE_CURRENT_SOURCE_DIR}/source/common")
AddIncludeDir(part2v1 "${CMAKE_CURRENT_SOURCE_DIR}/source/part2v1")

AddIncludeDir(part2v2 "${CMAKE_CURRENT_SOURCE_DIR}/source/common")
AddIncludeDir(part2v2 "${CMAKE_CURRENT_SOURCE_DIR}/source/part2v2")


# ----------------------------------- #
#     Linux specific configuration    #
# ----------------------------------- #

if (UNIX)
    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -DDEBUG")
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -DRELEASE")
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} --std=c++11")
endif()



# ------------------------------ #
#   OSX specific configuration   #
# ------------------------------ #

if (APPLE)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -I/opt/local/include/")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -L/opt/local/lib/")
endif()



# ------------------------------ #
# Windows specific configuration #
# ------------------------------ #

if (WIN32)
endif()
