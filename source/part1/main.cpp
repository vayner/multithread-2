/*
 *  IMT3801 Multi-threaded Programming - Task 2.1
 *
 *   121240 - 12HBSPA
 *   Tellef M�llerup �mdal
 */

#include <stdlib.h>

#include <random>
#include <thread>
#include <vector>
#include <array>

const int ITERATIONS = 20000000;
const int MAX_THREADS = 4;

std::random_device rd;
std::default_random_engine re(rd());
std::uniform_real_distribution<double> rnd(0.0, 1.0);

std::array<int, MAX_THREADS> resultList;


/**
 * @fn  long long reduction();
 *
 * @brief   Reduction time test.
 *
 * @return  The time used in microseconds.
 */

long long reduction() {
    int hits = 0;

    auto before = std::chrono::high_resolution_clock::now();

    int perThread = ITERATIONS / MAX_THREADS;
    int rest = ITERATIONS - (perThread * MAX_THREADS);
    std::vector<std::thread> threads;

    for (int i = 0; i < MAX_THREADS; i++) {
        threads.push_back(std::thread([=] {
            int start = perThread * i;
            int end = start + perThread;
            resultList[i] = 0;

            for (int j = start; j < end; j++) {
                double a = rnd(re);
                double b = rnd(re);
                resultList[i] += (a * a + b * b < 1.) ? 1 : 0;
            }

            printf("Thread %i done with %i to %i\n", i, start, end);
        }));
    }

    for (auto &t : threads) {
        t.join();
    }

    for (int res : resultList) {
        hits += res;
    }

    auto after = std::chrono::high_resolution_clock::now();
    auto diff = after - before;

    double approx = ((double) hits / (double) ITERATIONS) * 4.;

    printf("Approximated Pi to: %lf\n", approx);

    return std::chrono::duration_cast<std::chrono::microseconds>(diff).count();
}

/**
* @fn  int main()
*
* @brief   Main entry-point for this application.
*
* @return  Exit-code for the process - 0 for success, else an error code.
*/

int main() {

    long long reductionTime = reduction();

    printf("Reduction: %12lld\n", reductionTime);

    return 0;
}