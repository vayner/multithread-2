/*
 *  IMT3801 Multi-threaded Programming - Task 2.2 v1
 *
 *   121240 - 12HBSPA
 *   Tellef M�llerup �mdal
 */

#include <stdlib.h>

#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <string>
#include <queue>
#include <atomic>

std::atomic<bool> quit {false};

std::queue<long long> toMultiplier;
std::queue<long long> toPrinter;

std::mutex mMultiplier;
std::mutex mPrinter;

std::condition_variable cvMultiplier;
std::condition_variable cvPrinter;

/**
 * @fn  void ValueReader();
 *
 * @brief   Value reader thread.
 */

void ValueReader() {
    std::string input;
    long long value;

    while (true) {
        std::getline(std::cin, input);

        if(input.size() == 0) {
            continue;
        }

        if (input.at(0) == 'q') {
            break;
        }

        try {
            value = std::stoll(input);
        } catch (std::invalid_argument e) {
            printf("Not a number, ignoring\n");
            continue;
        } catch (std::out_of_range e) {
            printf("Too large a number, ignoring\n");
            continue;
        }

        printf("Sending: %lld\n", value);

        std::unique_lock<std::mutex> lock(mMultiplier);
        toMultiplier.push(value);
        lock.unlock();
        cvMultiplier.notify_one();
    }

    printf("ValueReader() quiting\n");

    quit.store(true);
    cvMultiplier.notify_all();
}

/**
 * @fn  void ValueMultiplier();
 *
 * @brief   Value multiplier thread.
 */

void ValueMultiplier() {
    long long value;

    while (!quit) {
        std::unique_lock<std::mutex> fromLock(mMultiplier);
        if(toMultiplier.size() == 0) {
            cvMultiplier.wait(fromLock, [] {return quit || toMultiplier.size() > 0; });
            continue;
        } else {
            value = toMultiplier.front();
            toMultiplier.pop();
            fromLock.unlock();
        }


        printf("Turned %lld into %lld\n", value, value * 3);
        value *= 3;


        std::unique_lock<std::mutex> toLock(mPrinter);
        toPrinter.push(value);
        toLock.unlock();
        cvPrinter.notify_one();
    }

    printf("ValueMultiplier() quiting\n");

    cvPrinter.notify_all();
}

/**
 * @fn  void ValuePrinter();
 *
 * @brief   Value printer thread.
 */

void ValuePrinter() {
    long long value;

    while (!quit) {
        std::unique_lock<std::mutex> fromLock(mPrinter);
        if(toPrinter.size() == 0) {
            cvPrinter.wait(fromLock, [] {return quit || toPrinter.size() > 0; });
            continue;
        } else {
            value = toPrinter.front();
            toPrinter.pop();
            fromLock.unlock();
        }

        printf("Printing %lld\n", value);
    }

    printf("ValuePrinter() quiting\n");
}

/**
 * @fn  int main()
 *
 * @brief   Main entry-point for this application.
 *
 * @return  Exit-code for the process - 0 for success, else an error code.
 */

int main() {

    std::thread input(ValueReader);
    std::thread multiplier(ValueMultiplier);
    std::thread printer(ValuePrinter);

    input.join();
    multiplier.join();
    printer.join();

    return 0;
}
