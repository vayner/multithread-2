/*
 *  IMT3801 Multi-threaded Programming - Task 2.2 v2
 *
 *   121240 - 12HBSPA
 *   Tellef M�llerup �mdal
 */

#include <stdlib.h>

#include <iostream>
#include <thread>
#include <future>
#include <atomic>
#include <string>

std::atomic<bool> quit {false};

std::future<long long> toMultiplier;
std::future<long long> toPrinter;

/**
* @fn  void ValueReader();
*
* @brief   Value reader thread.
*/

void ValueReader() {
    std::string input;
    long long value;

    while (true) {

        std::getline(std::cin, input);

        if(input.size() == 0) {
            continue;
        }

        if (input.at(0) == 'q') {
            break;
        }

        try {
            value = std::stoll(input);
        } catch (std::invalid_argument e) {
            printf("Not a number, ignoring\n");
            continue;
        } catch (std::out_of_range e) {
            printf("Too large a number, ignoring\n");
            continue;
        }

        printf("Sending: %lld\n", value);
        std::promise<long long> promise;
        promise.set_value(value);

        toMultiplier = promise.get_future();
    }

    printf("ValueReader() quiting\n");

    quit.store(true);
}

/**
* @fn  void ValueMultiplier();
*
* @brief   Value multiplier thread.
*/

void ValueMultiplier() {
    long long value;

    while (!quit) {
        if(!toMultiplier.valid()) {
            continue;
        }

        toMultiplier.wait_for(std::chrono::seconds(1));

        if(quit) {
            break;
        }


        value = toMultiplier.get();

        printf("Turned %lld into %lld\n", value, value * 3);
        value *= 3;


        std::promise<long long> promise;
        promise.set_value(value);

        toPrinter = promise.get_future();
    }

    printf("ValueMultiplier() quiting\n");
}

/**
* @fn  void ValuePrinter();
*
* @brief   Value printer thread.
*/

void ValuePrinter() {
    long long value;

    while (!quit) {
        if(!toPrinter.valid()) {
            continue;
        }

        toPrinter.wait_for(std::chrono::seconds(1));

        if(quit) {
            break;
        }

        value = toPrinter.get();

        printf("Printing %lld\n", value);
    }

    printf("ValuePrinter() quiting\n");
}

/**
* @fn  int main()
*
* @brief   Main entry-point for this application.
*
* @return  Exit-code for the process - 0 for success, else an error code.
*/

int main() {

    std::thread input(ValueReader);
    std::thread multiplier(ValueMultiplier);
    std::thread printer(ValuePrinter);

    input.join();
    multiplier.join();
    printer.join();

    return 0;
}
